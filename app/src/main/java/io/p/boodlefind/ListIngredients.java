package io.p.boodlefind;

import android.widget.Button;

/**
 * Created by Jairus on 9/26/2015.
 */
public class ListIngredients {

    private String _ingredients;

    public ListIngredients (String ingredients){
        _ingredients = ingredients;
    }

    public String getIngredients(){
        return _ingredients;
    }
}
