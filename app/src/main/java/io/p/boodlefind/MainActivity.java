package io.p.boodlefind;

import android.content.Intent;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import java.lang.StringBuilder;

public class MainActivity extends AppCompatActivity {

    EditText ingredient;
    List<ListIngredients> ListIngredient = new ArrayList<ListIngredients>();
    ListView listIngredientsView;
    String shrimpChecker = "";
    //StringBuilder ingredientsString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ingredient = (EditText) findViewById(R.id.editText);
        listIngredientsView = (ListView) findViewById(R.id.listView);
        final Button addBtn = (Button) findViewById(R.id.button2);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addListIngredient(ingredient.getText().toString());
                if(ingredient.getText().toString().equalsIgnoreCase("Shrimp")){
                    shrimpChecker = "Shrimp";
                }
                populateList();
                ingredient.setText("");
                //ingredientsString.append(ingredient.getText().toString());

            }

            public String removeSpaces() {
                StringBuilder temp = new StringBuilder(ingredient.getText().toString());
                for (int i = 0; i < temp.length(); i++) {
                    if (temp.charAt(i) == ' ') {
                        for (int a = i; a < temp.length(); a++) {
                            temp.deleteCharAt(a);
                        }
                    }
                    i--;
                }
                return temp.toString();
            }

        });


        final Button makeBtn = (Button) findViewById(R.id.button);
        makeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shrimpChecker.equalsIgnoreCase("Shrimp")) {
                    Intent intent = new Intent(MainActivity.this, SearchRecipe.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this, SearchRecipe2.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void populateList(){
        ArrayAdapter<ListIngredients> adapter = new ListIngredientsAdapter();
        listIngredientsView.setAdapter(adapter);
    }

    private void addListIngredient(String ingredients){
        ListIngredient.add(new ListIngredients(ingredients));
    }

    private class ListIngredientsAdapter extends ArrayAdapter<ListIngredients>{
        public ListIngredientsAdapter(){
            super(MainActivity.this, R.layout.item_list, ListIngredient);
        }


        @Override
        public View getView(final int position, View view, ViewGroup parent){
            if(view == null)
                view = getLayoutInflater().inflate(R.layout.item_list, parent, false);

            ListIngredients currentIngredients = ListIngredient.get(position);
            TextView  ingredients = (TextView) view.findViewById(R.id.ingredientFirst);
            ingredients.setText(currentIngredients.getIngredients());

            return view;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
